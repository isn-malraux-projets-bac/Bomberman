#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 10:31:19 2018

@author: utilisateur
"""

import pygame #permet d'utiliser le module pygame
import random #permet d'utiliser l'aléatoire
from pygame.locals import * #permet d'utiliser toutes les fonctions de pygame

matrice_jeu = [['m','m','m','m','m','m','m','m','m','m','m','m','m','m','m'],
               ['m','v','v','c','c','v','v','v','v','v','v','c','c','v','m'],
               ['m','v','m','v','m','v','m','v','m','c','m','c','m','v','m'],
               ['m','v','v','v','v','v','v','v','v','c','v','c','c','v','m'],
               ['m','c','m','v','m','v','m','v','m','c','m','v','m','v','m'],
               ['m','c','v','v','v','v','v','v','v','v','v','v','v','v','m'],
               ['m','c','m','c','m','v','m','s','m','v','m','c','m','v','m'],
               ['m','v','c','c','v','c','v','v','v','v','v','v','c','v','m'],
               ['m','c','m','v','m','c','m','c','m','v','m','v','m','v','m'],
               ['m','v','c','c','v','v','v','v','v','v','v','c','v','c','m'],
               ['m','v','m','c','m','c','m','v','m','v','m','v','m','v','m'],
               ['m','v','c','c','v','v','v','c','v','v','v','v','v','v','m'],
               ['m','m','m','m','m','m','m','m','m','m','m','m','m','m','m']]
#représentation du niveau case par case, avec une lettre pour chaque type de case,
                #m pour mur, v pour vide et s pour sortie
fond = pygame.image.load ("fond_vert.png")
mur = pygame.image.load ("Mur.png")
perso = pygame.image.load("carrotman.png")
sortie = pygame.image.load("sortie.png")
bombe = pygame.image.load("bombe.png")
explosion = pygame.image.load("explosion.png")
mechant = pygame.image.load("lapin.png")
cassable = pygame.image.load("mur_cassable.png")
gameOver = pygame.image.load("game over.png")
posMechant = mechant.get_rect()
posMechant.center = 48*1.5,672-24
position_perso = perso.get_rect()
posExplosion = explosion.get_rect()
lignePerso = 1
colonnePerso = 1
ligneMechant = 1
colonneMechant = 13
position_perso.center = 48*1.5,48*1.5
mouvEnnemi = 0
bombino = 0
temps_bombe = 0
temps_mtn = 0
largeur_fenetre = 720
hauteur_fenetre = 624
nbrEnnemi = 1
nb_colonne = 15
nb_ligne = 13

hauteur_case = largeur_fenetre/nb_colonne
pygame.mixer.pre_init(44100, -16, 2, 2048)#regle le délai des bruitages
pygame.mixer.init()   #lance le module de son
son_pas = pygame.mixer.Sound("son_pas.wav")
son_bombe = pygame.mixer.Sound("son_bombe.wav")
son_explosion = pygame.mixer.Sound("son_explosion.wav")


pygame.init() # lance pygame

icone = pygame.image.load("carrotman.png") #image de l'icone du jeu

pygame.display.set_icon(icone)  #affichage de cette icone 

pygame.display.set_caption("Carrotman vs zombie rabbits") #titre de la fenetre


fenetre = pygame.display.set_mode((hauteur_fenetre, largeur_fenetre)) #création de la fenetre
RAFRAICHIR_ECRAN = USEREVENT+1 #
pygame.time.set_timer(RAFRAICHIR_ECRAN, 500) #commandes qui executent la fonction de rafraichissement toutes les ms
MOUV_LAPIN = USEREVENT+2
pygame.time.set_timer(MOUV_LAPIN, 1000)#execution des mouvements du lapin toutes les secondes


def accueil():
    """
    Fonction qui affiche l'écran d'accueil
    
    Usage:
    >>>Lancer le programme:
    Affiche l'écran d'accueil
    raffraichirEcran
    Conditions initiales : Aucune
    Argument: aucun
    Sortie: aucun
    """ 
    pass

def rafraichirEcran():
    """
    Fonction qui affiche la carte, les murs, et la bombe.
    
    Usage:
    >>>Lancer le programme:
    affiche les éléments à l'écran
    rafraichirEcran
    Conditions initiales : Aucune
    Argument: aucun
    Sortie: aucun
    """ 
    global continuer
    global nbrEnnemi
    global bombino
    fenetre.blit(fond, (0,0)) #fait apparaitre le fond dans la fenetre
    if nbrEnnemi == 1:    
        fenetre.blit(mechant, posMechant)#fait apparaitre le lapin
    for ligne in range (nb_ligne) :
        for colonne in range (nb_colonne):
            if matrice_jeu[ligne][colonne] == 'm' :
                x_mur = hauteur_case * ligne
                y_mur = hauteur_case * colonne
                fenetre.blit( mur,( x_mur, y_mur) ) #double boucle pour faire apparaitre les murs
            
            if matrice_jeu[ligne][colonne] == 's' :
                x_sortie = hauteur_case * ligne 
                y_sortie = hauteur_case * colonne
                fenetre.blit(sortie,(x_sortie,y_sortie)) # "" "" "" la sortie
            
            if matrice_jeu[ligne][colonne] == 'c' :
                x_cassable = hauteur_case * ligne
                y_cassable = hauteur_case * colonne
                fenetre.blit(cassable, (x_cassable, y_cassable)) # "" "" "" les murs cassables

            #if deplacementMechant >= mouvEnnemi+2000 :
                #mouvEnnemi = deplacementMechant
                #if matrice_jeu[ligneMechant][colonneMechant - 1] == 'v':
                #posMechant = posMechant.move(0,(-hauteur_case))
                #colonneMechant = colonneMechant - 1

            
            if bombino == 1 : 
                temps_mtn = pygame.time.get_ticks()
                if temps_mtn <= temps_bombe + 3000 :
                    fenetre.blit(bombe,(ligne_bombe*hauteur_case ,colonne_bombe*hauteur_case)) # fait apparaitre la bombe tant qu'elle n'a pas été affichée 3s
                if temps_bombe+3000 <= temps_mtn <=temps_bombe + 6000:
                    son_explosion.play()
                    for compteur_abscisses in range (-2,3):
                        ligne_explosion = ligne_bombe
                        colonne_explosion = colonne_bombe + compteur_abscisses 
                        fenetre.blit(explosion,(ligne_explosion *hauteur_case,colonne_explosion*hauteur_case))  
                        if matrice_jeu[ligne_explosion][colonne_explosion] == 'c' :
                            matrice_jeu[ligne_explosion][colonne_explosion] = 'v'  #fait apparaitre les explosions et disparaitre les murs cassables
                                    
                            
                            
                    for compteur_ordonnées in range (-2,3):
                        ligne_explosion = ligne_bombe + compteur_ordonnées
                        colonne_explosion = colonne_bombe
                        fenetre.blit(explosion,(ligne_explosion * hauteur_case , colonne_explosion * hauteur_case ))
                        if matrice_jeu[ligne_explosion][colonne_explosion] == 'c':
                            matrice_jeu[ligne_explosion][colonne_explosion] = 'v'
                    if posExplosion.contains(posMechant):
                        nbrEnnemi = 0
                        
                    bombino = 0
    fenetre.blit(perso, position_perso)
    if position_perso.contains(posMechant):
        fenetre.blit(gameOver, (0,0)) #si le perso et la lapin se rencontrent, game over
        continuer = 0
        

    pygame.display.flip()

    
def mouvementLapin():
    """
    Fonction qui permet de bouger l'ennemi toutes les 2 secondes
    
    Usage:
    >>>Attendre 2 secondes
    Bouge l'ennemi dans une direction aléatoire
    
    Conditions initiales : Aucune
    Argument: aucun
    Sortie: aucun
    """ 
    global posMechant
    global colonneMechant
    global ligneMechant
    global continuer
    directMouv = random.randint(1,4)
    if directMouv == 1:
        print (directMouv)
        if matrice_jeu[ligneMechant][colonneMechant + 1] == 'v' :
            posMechant = posMechant.move(0,hauteur_case)
            colonneMechant = colonneMechant + 1
    elif directMouv == 2:
        print (directMouv)
        if matrice_jeu[ligneMechant + 1][colonneMechant] == 'v':
            posMechant = posMechant.move(hauteur_case,0)
            ligneMechant = ligneMechant + 1
    elif directMouv == 3:
        print (directMouv)
        if matrice_jeu[ligneMechant][colonneMechant - 1] == 'v' :
            posMechant = posMechant.move(0,(-hauteur_case))
            colonneMechant = colonneMechant - 1
    elif directMouv == 4:
        print (directMouv)
        if matrice_jeu[ligneMechant - 1][colonneMechant] == 'v' :
            posMechant = posMechant.move((-hauteur_case),0)
            ligneMechant = ligneMechant - 1

                    

rafraichirEcran()
pygame.key.set_repeat(400, 30)
mouvEnnemi = 0
continuer = 1
while continuer:
    
    
    for event in pygame.event.get():
        if event.type == RAFRAICHIR_ECRAN:
            rafraichirEcran()
        if event.type == MOUV_LAPIN:
            mouvementLapin()
        if event.type == QUIT:
            continuer = 0
        if event.type == KEYDOWN:
            
            if event.key == K_DOWN:
                if matrice_jeu[lignePerso][colonnePerso + 1] == 'v' :
                    position_perso = position_perso.move(0,hauteur_case)
                    colonnePerso = colonnePerso + 1
                    son_pas.play()
                    rafraichirEcran()
                    
                elif matrice_jeu[lignePerso][colonnePerso + 1] == 's' :
                    position_perso = position_perso.move(0,hauteur_case)
                    colonnePerso = colonnePerso + 1
                    son_pas.play()
                    rafraichirEcran()
                    
                     
            elif event.key == K_RIGHT:
                if matrice_jeu[lignePerso + 1][colonnePerso] == 'v':  
                    position_perso = position_perso.move(48,0)
                    lignePerso = lignePerso + 1
                    son_pas.play()
                    rafraichirEcran()
                    
                elif matrice_jeu[lignePerso + 1][colonnePerso] == 's' :
                    position_perso = position_perso.move(hauteur_case,0)
                    lignePerso = lignePerso + 1
                    son_pas.play()
                    rafraichirEcran()
                    
            elif event.key == K_UP:
                if matrice_jeu[lignePerso][colonnePerso - 1] == 'v':
                    position_perso = position_perso.move(0,(-hauteur_case))
                    colonnePerso = colonnePerso - 1
                    son_pas.play()
                    rafraichirEcran()
                    
                elif matrice_jeu[lignePerso][colonnePerso - 1] == 's' :
                    position_perso = position_perso.move(0,-hauteur_case)
                    colonnePerso = colonnePerso - 1
                    son_pas.play()
                    rafraichirEcran()
                    
            elif event.key == K_LEFT:
                if matrice_jeu[lignePerso - 1][colonnePerso] == 'v' :
                    position_perso = position_perso.move((-hauteur_case),0)
                    lignePerso = lignePerso - 1
                    son_pas.play()
                    rafraichirEcran()
                    
                elif matrice_jeu[lignePerso - 1][colonnePerso] == 's' :
                    position_perso = position_perso.move(-hauteur_case,0)
                    lignePerso = lignePerso - 1
                    son_pas.play()
                    rafraichirEcran()
                    
            elif event.key == K_SPACE:
                if bombino == 0:
                    son_bombe.play()
                    temps_bombe = pygame.time.get_ticks()
                    colonne_bombe = colonnePerso
                    ligne_bombe = lignePerso
                    bombino = 1
                    rafraichirEcran()
                
            
   



pygame.quit()
